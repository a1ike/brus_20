$(document).ready(function () {

  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.b-modal').toggle();
  });

  $('.b-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'b-modal__centered') {
      $('.b-modal').hide();
    }
  });

  $('.b-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.b-modal').hide();
  });

  $('.b-header__toggle').on('click', function (e) {
    e.preventDefault();

    $('.b-header__top').slideToggle('fast');
    $('.b-header__main').slideToggle('fast');
  });

  $('.b-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.b-tabs__header li').removeClass('current');
    $('.b-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  new Swiper('.b-home-stocks', {
    slidesPerView: 1,
    pagination: {
      el: '.b-home-stocks .swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.b-home-stocks .swiper-button-next',
      prevEl: '.b-home-stocks .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 2,
        spaceBetween: 60,
      },
    }
  });

  new Swiper('.b-home-reviews__cards', {
    slidesPerView: 1,
    pagination: {
      el: '.b-home-reviews__cards .swiper-pagination',
      clickable: true,
    },
    navigation: {
      nextEl: '.b-home-reviews__cards .swiper-button-next',
      prevEl: '.b-home-reviews__cards .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 60,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 60,
      },
    }
  });

  new Swiper('.b-papers__cards', {
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.b-papers__next',
      prevEl: '.b-papers__prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
        spaceBetween: 60,
      },
      1200: {
        slidesPerView: 3,
        spaceBetween: 60,
      },
    }
  });

  new Swiper('.b-good__cards', {
    pagination: {
      el: '.b-good__cards .swiper-pagination',
      clickable: true,
    },
  });
});
